import ConfigManager from "./config-manager";

export default class WebExtensionsConfigManager extends ConfigManager {

    build() {
        this.configHandler({"tabs": ["formatted", "raw"]})
    }
}