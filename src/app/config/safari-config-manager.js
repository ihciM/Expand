import ConfigManager from "./config-manager";

export default class SafariConfigManager extends ConfigManager {
    handleConfigMessage(event) {
        if (event.name === "config") {
            this.configHandler(event.message);
        }
    }

    build() {
        safari.self.addEventListener("message", (event) => this.handleConfigMessage(event))
        safari.extension.dispatchMessage("requestConfig")
    }
}