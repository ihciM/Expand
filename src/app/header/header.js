import Mustache from "mustache";
import header from "../../templates/header/header.mustache";
import headerStylesheet from "../../styles/header/header.scss";

export default class Header {
    constructor(tabs) {
        this.tabs = tabs;
    }

    addAllListeners() {
        let tabLinks = document.querySelectorAll("nav .tab-link");
        tabLinks.forEach((tabLink, index) => this.addListener(tabLink, index));
    }

    addListener(tabLink, index) {
        tabLink.addEventListener("click", () => this.setTab(index));
    }

    setTabClasses(item, indexOfItem, indexOfCurrentTab) {
        item.classList.toggle('active', indexOfCurrentTab === indexOfItem)
        item.classList.toggle('right', indexOfItem > indexOfCurrentTab)
        item.classList.toggle('left', indexOfItem < indexOfCurrentTab)
    }

    setTab(index) {
        let leftTabLinkWidthSum = 0;
        let rightTabLinkWidthSum = 0;
        let leftTabLinkCount = 0;
        let rightTabLinkCount = 0;

        let tabLinks = document.querySelectorAll("nav .tab-link");
        tabLinks.forEach((tabLink, tabIndex) => {
            this.setTabClasses(tabLink, tabIndex, index)
            if (tabIndex < index) {
                leftTabLinkWidthSum += tabLink.offsetWidth;
                leftTabLinkCount++;
            } else if (tabIndex > index) {
                rightTabLinkCount++;
                rightTabLinkWidthSum += tabLink.offsetWidth;
            }
        });

        document.getElementsByClassName('background')[0].style.paddingLeft = `calc(${leftTabLinkWidthSum}px + ${leftTabLinkCount * .5}rem + .25rem)`;
        document.getElementsByClassName('background')[0].style.paddingRight = `calc(${rightTabLinkWidthSum}px + ${rightTabLinkCount * .5}rem + .25rem)`;

        let tabs = document.querySelectorAll(".container .tab");
        tabs.forEach((tab, tabIndex) => {
            this.setTabClasses(tab, tabIndex, index)
        });
    }

    build() {
        headerStylesheet.use();

        let result = Mustache.render(header, {tabs: this.tabs});
        document.body.innerHTML += result;

        this.addAllListeners();
    }
}