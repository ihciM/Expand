import hljs from 'highlight.js/lib/core';
import json from 'highlight.js/lib/languages/json';
import JsonViewerApplication from "./json-viewer-application";
import WebExtensionsConfigManager from "./config/web-extensions-config-manager";

hljs.registerLanguage('json', json);

if (document.contentType === "application/json") {
    let dataExtractor = () => document.body.firstChild.textContent;
    let configManagerFactory = (callback) => new WebExtensionsConfigManager(callback)

    let application = new JsonViewerApplication(dataExtractor, configManagerFactory);

    application.build();
}

