import TabManager from "./tabs/tabs";
import ThemeManager from "./theme/theme-manager";
import ConfigManager from "./config/config-manager";
import Header from "./header/header";
import jsonBase from "../styles/base/base.scss";
import fontawesome from "@fortawesome/fontawesome-free/css/all.min.css";

export default class JsonViewerApplication {
    constructor(dataExtractor, configManagerFactory) {
        this.rawDataAsString = dataExtractor();
        this.configManager = configManagerFactory((config) => this.buildInterface(config));
        this.components = []
    }

    loadBasicStyleSheets() {
        jsonBase.use();
        fontawesome.use();
    }

    clearDocument() {
        document.body.textContent = "";
    }

    buildInterface(config) {
        this.clearDocument();
        this.loadBasicStyleSheets();

        let tabManager = new TabManager(this.rawDataAsString, config.tabs);
        this.components = [
            new Header(tabManager.tabs),
            tabManager,
            new ThemeManager(),
        ]

        this.components.forEach(value => value.build());
        this.components[0].setTab(0);
    }

    build() {
        this.configManager.build();
    }
}