import Mustache from "mustache";
import rawJson from "../../../templates/tabs/raw/raw.mustache";
import hljs from "highlight.js/lib/core";

export default class RawTab {
    constructor(rawDataAsString) {
        this.name = "Raw";
        this.rawDataAsString = rawDataAsString;
    }

    build(parent) {
        let result = Mustache.render(rawJson, {rawData: this.rawDataAsString});
        parent.innerHTML += result;

        hljs.highlightElement(document.querySelector("#raw-json pre code"));
    }

    afterBuild() {

    }
}