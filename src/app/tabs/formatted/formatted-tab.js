import Mustache from "mustache";
import formattedJson from "../../../templates/tabs/formatted/formatted.mustache";
import hljs from "highlight.js/lib/core";
import ExpansionHandler from "../expansion-handler";

export default class FormattedTab {
    constructor(formattedDataAsString) {
        this.name = "Formatted"
        this.formattedDataAsString = formattedDataAsString;
        this.expansionHandler = new ExpansionHandler(
            (element) => element.parentElement,
            (element) => element.children[0],
            () => document.querySelectorAll("#formatted-json .caret-wrapper"),
            true,
            (element, isExpanded) => {
                if (isExpanded) {
                    let oldValue = element.children[1].textContent;
                    let newValue = oldValue.replace(/\[\.\.\.]$/, "[");
                    newValue = newValue.replace(/{\.\.\.}$/, "{");
                    element.children[1].textContent = newValue;
                } else {
                    let oldValue = element.children[1].textContent;
                    let newValue = oldValue.replace(/\[$/, "[...]");
                    newValue = newValue.replace(/{$/, "{...}");
                    element.children[1].textContent = newValue;
                }
            }
        )
    }

    build(parent) {
        let lines = [];
        let lastIndentLevel = -1;

        this.formattedDataAsString.split('\n')
            .forEach((value, index) => {
                let indentLevel = value.search(/\S|$/) / 2

                if (lastIndentLevel !== -1 && indentLevel > lastIndentLevel) {
                    lines[lines.length - 1].isExpandable = true;
                }

                let valueWithoutSpaces = value.replace(/\s/g, '');
                let isClosing = valueWithoutSpaces === ']' || valueWithoutSpaces === '}' ||
                    valueWithoutSpaces === '],' || valueWithoutSpaces === '},';

                lines.push({
                    content: value,
                    indentLevel: indentLevel,
                    isExpandable: false,
                    isClosing: isClosing
                });

                lastIndentLevel = indentLevel;
            });

        let result = Mustache.render(formattedJson, {lines: lines});

        parent.innerHTML += result;

        hljs.highlightElement(document.querySelector("#formatted-json pre code"));
    }

    afterBuild() {
        this.expansionHandler.build()
    }
}