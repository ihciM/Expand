import Mustache from "mustache";
import error from "../../../templates/tabs/error/error.mustache";

export default class ErrorTab {
    constructor(errorMessage) {
        this.name = "Error";
        this.errorMessage = errorMessage
    }

    build(parent) {
        let result = Mustache.render(error, {errorMessage: this.errorMessage});
        parent.innerHTML += result;
    }

    afterBuild() {

    }
}