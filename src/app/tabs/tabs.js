import FormattedTab from "./formatted/formatted-tab";
import RawTab from "./raw/raw-tab";
import DataTab from "./data/data-tab";
import jsonTab from "../../styles/tabs/tabs.scss";
import ErrorTab from "./error/error-tab";

export default class TabManager {
    constructor(rawDataAsString, tabs) {
        this.rawDataAsString = rawDataAsString;
        try {
            this.data = JSON.parse(this.rawDataAsString);
            this.formattedDataAsString = JSON.stringify(this.data, null, 2);
        } catch (error) {
            this.errorMessage = error.message;
            tabs = ["error", "raw"]
        }

        this.tabs = tabs.map(tab => {
            switch (tab) {
                case "error":
                    return new ErrorTab(this.errorMessage)
                case "data":
                    return new DataTab(this.data)
                case "formatted":
                    return new FormattedTab(this.formattedDataAsString)
                case "raw":
                    return new RawTab(this.rawDataAsString)
            }
        })
    }

    build() {
        jsonTab.use();

        let container = document.createElement('div');
        container.classList.add('container');
        document.body.appendChild(container);

        this.tabs.forEach(tab => tab.build(container));
        this.tabs.forEach(tab => tab.afterBuild());
    }
}
