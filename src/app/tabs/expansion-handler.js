export default class ExpansionHandler {
    constructor(getRowFromButton, getButtonFromRow, getAllButtons, toggleNextElement, adjustLineContent) {
        this.getRowFromButton = getRowFromButton;
        this.getButtonFromRow = getButtonFromRow;
        this.getAllButtons = getAllButtons;
        this.toggleNextElement = toggleNextElement;
        this.adjustLineContent = adjustLineContent;
    }

    addCaretListeners() {
        let carets = this.getAllButtons();

        carets.forEach((element, index) => this.addOpenOrCloseChildrenListener(element));
    }

    addOpenOrCloseChildrenListener(element) {
        element.addEventListener("click", () => this.openOrCloseChildren(element));
    }

    openOrCloseChildren(element) {
        element.classList.toggle('active');
        this.updateChildren(this.getRowFromButton(element));
    }

    updateChildren(element) {
        let active = this.getButtonFromRow(element).classList.contains('active');
        let indentLevel = element.dataset.indentLevel;

        this.adjustLineContent(element, active);

        let currentElement = element;

        while (currentElement.nextElementSibling !== null && currentElement.nextElementSibling.dataset.indentLevel > indentLevel) {
            currentElement = currentElement.nextElementSibling;

            if (active) {
                currentElement.classList.add('active');
                if (currentElement.dataset.isObject === 'true') {
                    currentElement = this.updateChildren(currentElement);
                }
            } else {
                currentElement.classList.remove('active');
            }
        }

        if (this.toggleNextElement) {
            if (active) {
                currentElement.nextElementSibling.classList.add('active');
            } else {
                currentElement.nextElementSibling.classList.remove('active');
            }
            return currentElement.nextElementSibling;
        }

        return currentElement;
    }

    build() {
        this.addCaretListeners();
        [...this.getAllButtons()].map(button => this.getRowFromButton(button))
            .filter(b => b.dataset.isObject && b.dataset.indentLevel == 0)
            .forEach((row, index) => this.updateChildren(row))
    }
}