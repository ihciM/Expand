import Mustache from "mustache";
import expandableJson from "../../../templates/tabs/data/table.mustache";
import expandableEntry from "../../../templates/tabs/data/entry.mustache";
import ExpansionHandler from "../expansion-handler";

export default class DataTab {
    constructor(data) {
        this.name = "Data";
        this.data = data;
        this.expansionHandler = new ExpansionHandler(
            (element) => element.parentElement.parentElement.parentElement,
            (element) => element.children[0].children[0].children[0],
            () => document.querySelectorAll("#expandable-json .caret-wrapper"),
            false,
            (element, isExpanded) => {}
        );
    }

    applyIndentLevels() {
        let elements = document.querySelectorAll(".entry[data-indent-level] .key");
        elements.forEach((element, index) => {
            let indentLevel = element.parentElement.dataset.indentLevel;
            element.style.paddingLeft = `${indentLevel}rem`;
        });
    }

    getValueClass(value) {
        switch (typeof (value)) {
            case 'number':
                return 'hljs-number';
            case 'string':
                return 'hljs-string';
            default:
                return 'hljs-literal';
        }
    }

    createValueForTemplate(passedValue, indentLevel) {
        let entries = [];

        for (const [key, value] of Object.entries(passedValue)) {
            if (typeof (value) == 'object' && value != null) {
                entries.push({
                    key: key,
                    value: "{...}",
                    indentLevel: indentLevel,
                    isObject: true,
                    valueClass: "hljs-punctuation"
                })
                entries = entries.concat(this.createValueForTemplate(value, indentLevel + 1));
            } else {
                entries.push({
                    key: key,
                    value: JSON.stringify(value),
                    indentLevel: indentLevel,
                    isObject: false,
                    valueClass: this.getValueClass(value)
                });
            }
        }

        return entries;
    }

    build(parent) {
        let entries = this.createValueForTemplate(this.data, 0);

        let result = Mustache.render(expandableJson, {entries: entries}, {expandableEntry: expandableEntry});

        parent.innerHTML += result;

        this.applyIndentLevels();
    }

    afterBuild() {
        this.expansionHandler.build();
    }
}
