import hljs from 'highlight.js/lib/core';
import json from 'highlight.js/lib/languages/json';
import JsonViewerApplication from "./json-viewer-application";
import SafariConfigManager from "./config/safari-config-manager";

hljs.registerLanguage('json', json);

document.addEventListener("DOMContentLoaded", function (event) {
    if (document.contentType === "application/json") {
        let dataExtractor = () => document.body.firstChild.textContent;
        let configManagerFactory = (callback) => new SafariConfigManager(callback)

        let application = new JsonViewerApplication(dataExtractor, configManagerFactory);

        application.build();
    }
});

