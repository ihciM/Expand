import lightTheme from "highlight.js/styles/stackoverflow-light.css";
import darkTheme from "highlight.js/styles/stackoverflow-dark.css";

export default class ThemeManager {
    constructor() {
        this.themes = [
            {
                theme: lightTheme,
                isInUse: false
            },
            {
                theme: darkTheme,
                isInUse: false
            }
        ]
    }

    addThemeListener() {
        let matchMedia = window.matchMedia("(prefers-color-scheme: dark)");
        matchMedia.addEventListener("change", e => this.setTheme(e.matches));

        this.setTheme(matchMedia.matches);
    }

    unuseTheme(index) {
        if (this.themes[index].isInUse) {
            this.themes[index].theme.unuse();
        }

        this.themes[index].isInUse = false;
    }

    useTheme(index) {
        if (!this.themes[index].isInUse) {
            this.themes[index].theme.use();
        }

        this.themes[index].isInUse = true;
    }

    setTheme(isDark) {
        if (isDark) {
            this.unuseTheme(0)
            this.useTheme(1);
        } else {
            this.unuseTheme(1);
            this.useTheme(0);
        }
    }

    build() {
        this.addThemeListener();
    }
}