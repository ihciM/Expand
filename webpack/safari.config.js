const path = require('path');

module.exports = {
    entry: './src/app/safari.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, '../safari/dist')
    },
}