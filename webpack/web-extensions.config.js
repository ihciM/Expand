const path = require('path');

module.exports = {
    entry: './src/app/web-extensions.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, '../web-extensions/dist')
    },
}