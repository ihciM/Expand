const path = require('path');

module.exports = {
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [
                    {
                        loader: 'style-loader',
                        options: {injectType: 'lazyStyleTag'}
                    },
                    'css-loader'
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        loader: 'style-loader',
                        options: {injectType: 'lazyStyleTag'}
                    },
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.mustache$/i,
                type: 'asset/source'
            },
            {
                test: /\.(woff[2]?)|(ttf)|(eot)|(woff)|(svg)$/i,
                type: 'asset/inline'
            }
        ]
    },
};