# Expand

Expand is an Open-Source JSON-Viewer, currently offering the functionality of viewing a formatted version of JSON-Documents, with the ability to expand and collapse objects.

Furthermore it also displays the raw JSON-Data received.

# Usage

The JSON-Viewer will be present on any page with the ContentType: application/json

On Firefox, you have to set devtools.jsonview.enabled to false in about:config, otherwise the default JSON-Viewer will override this extension.

# Build

## Safari

Open the project with XCode and hit build.

The JavaScript-Application will be built automatically.

## Web-Extensions-API

`npm run build-web-extensions`