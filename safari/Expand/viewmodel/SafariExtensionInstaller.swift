//
//  ExtensionInstaller.swift
//  Expand
//
//  Created by Michael Gail on 09.06.21.
//

import Foundation
import SafariServices

class SafariExtensionInstaller: ObservableObject {
    
    @Published var safariExtension = SafariExtension(bundleIdentifier: "eu.michael-gail.Expand.Expand-Extension");
    
    init() {
        checkIfExtensionIsInstalled()
    }
    
    func checkIfExtensionIsInstalled() {
        SFSafariExtensionManager.getStateOfSafariExtension(withIdentifier: safariExtension.bundleIdentifier) { state, error in
            DispatchQueue.main.async {
                self.safariExtension.isEnabled = state?.isEnabled ?? false
            }
            
        }
    }
    
    var extensionBundleIdentifier: String {
        get {
            safariExtension.bundleIdentifier
        }
    }
    
    var extensionIsEnabled: Bool {
        get {
            safariExtension.isEnabled
        }
    }
}
