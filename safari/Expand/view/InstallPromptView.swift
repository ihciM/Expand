//
//  InstallPromptView.swift
//  Expand
//
//  Created by Michael Gail on 09.06.21.
//

import SwiftUI
import SafariServices

struct InstallPromptView: View {
    @EnvironmentObject var extensionInstaller: SafariExtensionInstaller
    
    var body: some View {
            VStack(alignment: .leading) {
                Text("1. Open Safari Settings")
                    .font(.title)
                    .padding()
                Text("2. Navigate to Extensions")
                    .font(.title)
                    .padding()
                Text("3. Click the checkmark to activate the extension")
                    .font(.title)
                    .padding()
                Button(action: {
                    SFSafariApplication.showPreferencesForExtension(withIdentifier: self.extensionInstaller.extensionBundleIdentifier) { error in
                    }
                }) {
                    Text("Open Safari Settings")
                }.padding()
            }
    }
}

struct InstallPromptView_Previews: PreviewProvider {
    static var previews: some View {
        InstallPromptView()
    }
}
