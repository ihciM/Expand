//
//  AppView.swift
//  Expand
//
//  Created by Michael Gail on 09.06.21.
//

import SwiftUI

struct AppView: View {
    var body: some View {
        VStack(alignment: .leading) {
            Image("Logo")
                .resizable()
                .frame(width: 75, height: 75)
                .padding()
            Text("You are all set!")
                .font(.largeTitle)
                .padding()
        }.padding()
    }
}

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        AppView()
    }
}
