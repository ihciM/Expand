//
//  ContentView.swift
//  Expand
//
//  Created by Michael Gail on 25.05.21.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var extensionInstaller: SafariExtensionInstaller
    
    var body: some View {
        if extensionInstaller.extensionIsEnabled {
            AppView()
                .fixedSize()
        } else {
            InstallPromptView()
                .fixedSize()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
