//
//  ExpandSafariExtension.swift
//  Expand
//
//  Created by Michael Gail on 09.06.21.
//

import Foundation

struct SafariExtension {
    
    var bundleIdentifier: String;
    var isEnabled = false
}
