//
//  ExpandApp.swift
//  Expand
//
//  Created by Michael Gail on 25.05.21.
//

import SwiftUI

@main
struct ExpandApp: App {
    @StateObject var extensionInstaller: SafariExtensionInstaller = SafariExtensionInstaller()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(extensionInstaller)
        }
    }
}
